var app = app || {};

(function () {
	'use strict';

	var Builds = Backbone.Collection.extend({
		model: app.Build,
		url: '/api',
		comparator: 'id',

		parse: function (response) {
            app.abnormals = response.meta.abnormals;
		    return response.builds;
		}
		
	});

	app.builds = new Builds();
})();
