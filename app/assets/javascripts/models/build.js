var app = app || {};

(function () {
    'use strict';

    app.Build = Backbone.Model.extend({
        defaults: {
            id: 0,
            summary_status: '',
            duration: 0,
            created_at: ''
        }

    });
})();
