var app = app || {};

(function () {
    'use strict';

    var BuildsRouter = Backbone.Router.extend({
        routes: {
            '(/)': 'index'
        },

        index: function () {
            //app.builds.trigger('someEvent');
        }
    });

    app.BuildsRouter = new BuildsRouter();

    Backbone.history.start({pushState: true});
})();
