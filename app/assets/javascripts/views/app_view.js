var app = app || {};

(function ($) {
    "use strict";

    app.AppView = Backbone.View.extend({
        el: ".charting-app",

         events: {
        	'click #reload-button': 'reloadCharts'
        },

        initialize: function () {
            this.$footer = $("footer");
            this.$firstChart = $(".first-chart-container");
            this.$secondChart = $(".second-chart-container");
            this.$thirdChart = $(".third-chart-container");

            this.listenTo(app.builds, "reset", this.render);

            window.google.charts.load("current", {'packages':["corechart"]});

            app.builds.fetch({reset: true});
        },

        render: function () {
            if (app.builds.length) {
                var firstChartDays = {};
                var firstChartRows = [];
                var secondChartRows = [];

                app.builds.each(function(build) {
                    var day = build.get("created_at").substr(0, 10);
              
                    if (firstChartDays[day] === undefined) {
                        firstChartDays[day] = {
                            'passed': 0,
                            'error': 0,
                            'failed': 0,
                            'stopped': 0,
                            'annotation': "",
                            'annotationText': ""
                        };

                        if (app.abnormals[day]) {
                            firstChartDays[day]["annotation"] = "Abnormal";
                            firstChartDays[day]["annotationText"] = 'Anomaly detected this day';
                        }
                    }

                    firstChartDays[day][build.get("summary_status")] += 1;

                    secondChartRows.push([ new Date(build.get("created_at")), build.get("duration") ]);

                    });

                _.each(firstChartDays, function(data, date) {
                    
                    firstChartRows.push([
                        new Date(date),
                        data["passed"],
                        data["failed"],
                        data["annotation"],
                        data["annotationText"],
                        data["error"],
                        data["stopped"]
                    ]);
                });

                google.charts.setOnLoadCallback(function() {
            
                    var firstChartData = new google.visualization.DataTable();
                    var secondChartData = new google.visualization.DataTable();

                    firstChartData.addColumn("date", "Date");
                    firstChartData.addColumn("number", "Passed");
                    firstChartData.addColumn("number", "Failed");
                    firstChartData.addColumn({type: "string", role: "annotation"});
                    firstChartData.addColumn({type: "string", role: "annotationText"});
                    firstChartData.addColumn("number", "Error");
                    firstChartData.addColumn("number", "Stopped");

                    
                    firstChartData.addRows(firstChartRows);

                    
                    secondChartData.addColumn("datetime", "When");
                    secondChartData.addColumn("number", "Duration");

                    
                    secondChartData.addRows(secondChartRows);

                    
                    new google.visualization.ColumnChart($(".first-chart")[0]).
                    draw(firstChartData,
                        {
                            
                            title:"Builds quality per day",
                            width: 1200, height: 300,
                            vAxis: {
                                title: "Builds"
                            }, isStacked: true,
                            hAxis: {title: "Date"},
                            colors: ["#109618", "#DC3912", "#990099", "#FF9900"],
                            bar: { groupWidth: "85%" },
                            annotations: {'column_id': {style: 'line'}},
                            pointSize: 30
                        }
                    );

                    
                    new google.visualization.LineChart($(".second-chart")[0]).
                    draw(secondChartData,
                        {
                            title:"Duration vs. time",
                            width: 1200, height: 300,
                            curveType: "function",
                            vAxis: {title: "Seconds"},
                            hAxis: {title: "DateTime", format: "MMM dd hh:mm"},
                            colors: ["#990099"]
                        }
                    );
                // dots chart
                    new google.visualization.ScatterChart($(".third-chart")[0]).
                    draw(firstChartData,
                        {
                            title: 'Error builds anomalities',
                            width: 1200, height: 300,
                            hAxis: {title: 'Date'},
                            vAxis: {title: 'Builds'},
                            curveType: 'function',
                            pointSize: 7,
                            pointShape: 'circle',
                            colors: ["#109618", "#DC3912", "#990099", "#FF9900"],
                        }
                    );
                });

           
                this.$firstChart.show();
                this.$secondChart.show();
                //this.$thirdChart.show();
                this.$footer.show();
            } else {
                this.$firstChart.hide();
                this.$secondChart.hide();
                //this.$thirdChart.show();
                this.$footer.hide();
            }

            return this;
        },

        reloadCharts: function() {
            app.builds.fetch({reset: true});
        }

    });

})(jQuery);
