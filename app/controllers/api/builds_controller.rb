class Api::BuildsController < ApplicationController
	PATH = Rails.root.join('session_history.csv')

  def index
	builds = Build.read(PATH)
	
	#render json: Build.read_from_file[:builds],
	render json: builds[:builds],
	  each_serializer: Api::BuildsSerializer,
	  meta: { abnormals: builds[:abnormals] }
  end
end
