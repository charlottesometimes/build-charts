class Build < ActiveModelSerializers::Model
  require 'csv'

  attr_accessor :id, :summary_status, :duration, :created_at

  alias :read_attribute_for_serialization :send

  ERROR_STATES = %w(error failed)
  SUCCESS_STATES = %w(passed)

  def initialize(options = {})
    @id = options[:id].to_i
    @summary_status = options[:summary_status].to_s
    @duration = options[:duration].to_i
    @created_at = options[:created_at].to_s
  end

  def self.read(file)
  	builds = []
  	# success_builds_per_day = Hash.new({})
  	error_builds_per_day = Hash.new({})
  	abnormal_days = {}
  	rows = 0

	CSV.foreach(file, headers: true) do |line|
	  if line["session_id"].blank? ||
		 line["summary_status"].blank? || 
		 line["duration"].blank? ||
		 line["created_at"].blank?
	  	next
	  end
	  builds << self.new(id: line["session_id"],
	  					 summary_status: line["summary_status"],
	  					 duration: line["duration"],
	  					 created_at: line["created_at"])
	  #day = Date.parse(Time.parse(line["created_at"]).to_s)
	  day = Time.parse(line["created_at"]).to_s[0..9]

    # builds_per_day[day] ||= {key_a: 0, cluster_num: nil, is_anomaly: false}
    #raise error_builds_per_day.inspect
    if error_builds_per_day[day] == {}
      error_builds_per_day[day] = {key_a: 0, cluster_num: nil, is_anomaly: false}
    end

	  # success_builds_per_day[day][:key_a] += 1 if SUCCESS_STATES.include?(line["summary_status"])
	  error_builds_per_day[day][:key_a] += 1 if ERROR_STATES.include?(line["summary_status"])

	  rows += 1

  end

  builds_per_day = Build.find_anomalies(error_builds_per_day)


# p success_builds_per_day
  builds_per_day.each do |day, data|
    abnormal_days[day] = data[:is_anomaly]
  end
	#End clustering
	#{success_builds_per_day: success_builds_per_day}

	# {j_index: j_index,
	# 	criteria: criteria,
	# 	centroids_show: centroids_show,
	# 	old_centroids: old_centroids,
	# 	centroids: centroids}
	 { builds: builds, abnormals: abnormal_days }
	#   error_builds_per_day: error_builds_per_day,
	#   success_builds_per_day: success_builds_per_day,
	#   rows: rows,
	#   centroids_show: centroids_show }
   #  nil
  end

  def self.find_anomalies(error_builds_per_day)
    #Start clustering
    #1 centroids determination
    k=5
    error_arr = error_builds_per_day.to_a

    z = error_arr.length
    step = (z/k).to_i
    centroids = []
    centroids << [error_arr[0][0], error_arr[0][1][:key_a]]
    for i in 1...k do
      centroids << [error_arr[i*step-1][0], error_arr[i*step-1][1][:key_a]]
    end
    centroids_show = centroids

    #statement
    old_centroids = nil
    criteria = false

    #old_centroids = nil || criteria
    j_index = 0
    #for calculating centroid date
    min_date = nil
    #j_index < 50 &&
    while (old_centroids.nil? || criteria) do
      j_index+=1
      old_centroids = centroids
      #2
      error_builds_per_day.each do |h|
        # h = hash of ALL dates as array[date, hash]
        if min_date.nil? || min_date> Date.parse(h[0])
          min_date = Date.parse(h[0])
        end

        min_d = nil
        cluster_index = 0
        centroids.each do |c|

          x_diff = (Date.parse(c[0].to_s) - Date.parse(h[0])).to_i

          y_diff = (c[1].to_i - h[1][:key_a].to_i).to_i

          delta = Math.sqrt(x_diff*x_diff + y_diff*y_diff)

          if min_d.nil? || delta < min_d

            min_d = delta


            error_builds_per_day[h[0]][:cluster_num]= cluster_index
          end
          cluster_index+=1
        end
      end
      #3 find new centroids
      centroids = []
      for i in 0...k do

        sum_x =0
        sum_y =0
        total_in_cluster = 0
        error_builds_per_day.each do |h|

          if h[1][:cluster_num] == i
            total_in_cluster +=1
            sum_x += (Date.parse(h[0]) - min_date).to_i
            sum_y += h[1][:key_a]
          end

        end

        centroids[i]=[min_date+(sum_x/total_in_cluster).to_i, sum_y/total_in_cluster]

      end

      #comparing centroids 2
      criteria = false
      if old_centroids.nil?
        criteria = true
      else
        i=0
        centroids.each do |e|

          old_e=old_centroids[i]

          if e[0].to_s != old_e[0].to_s || e[1] != old_e[1]
            criteria = true
          end
          i+=1
        end
      end
      #end comparing centroids 2
    end

    #cluster size
    clusters_size = {}
    for i in 0...k
      clusters_size[i] = 0
    end

    error_builds_per_day.each do |h|
      cn = h[1][:cluster_num]
      clusters_size[cn]+=1
    end

    # 5 find anomalies
    for i in 0...k do
      max_d=0
      c = centroids[i]
      date_of_max_distant=nil
      error_builds_per_day.each do |h|
        if h[1][:cluster_num] == i
          x_diff = (Date.parse(c[0].to_s) - Date.parse(h[0])).to_i
          y_diff = (c[1].to_i - h[1][:key_a].to_i).to_i
          delta = Math.sqrt(x_diff*x_diff + y_diff*y_diff)
          if delta > max_d
            max_d = delta
            date_of_max_distant = h[0]
          end
        end
      end
      # puts "success_builds_per_day[date_of_max_distant]: #{success_builds_per_day[date_of_max_distant]}"

     # #checking no zero abnormals
     #  if clusters_size[error_builds_per_day[date_of_max_distant][:cluster_num]] > 1
     #    error_builds_per_day[date_of_max_distant][:is_anomaly] = true
     #  end
      if clusters_size[error_builds_per_day[date_of_max_distant][:cluster_num]] > 1 && error_builds_per_day[date_of_max_distant][:key_a] > 1
        error_builds_per_day[date_of_max_distant][:is_anomaly] = true
      end

      #error_builds_per_day[date_of_max_distant][:is_anomaly] = true
      # p success_builds_per_day[date_of_max_distant]
      # p date_of_max_distant
      # p '-------------------------'
      # abnormal_days[day]= true
    end

    error_builds_per_day
  end
end
