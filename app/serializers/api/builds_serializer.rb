class Api::BuildsSerializer < ActiveModel::Serializer
  attributes :id, :summary_status, :duration, :created_at
end
