Rails.application.routes.draw do
  namespace :api, defaults: {format: 'json'} do
      get '/', to: 'builds#index'
  end
  
  root 'index#index'
  
  get '*path', to:'index#index'
end
