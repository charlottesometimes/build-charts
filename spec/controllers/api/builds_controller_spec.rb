require 'rails_helper'

RSpec.describe Api::BuildsController, type: :controller do
  describe 'index' do
    it 'should respond' do
      Api::BuildsController::PATH = Rails.root.join('spec', 'session_history.csv')
      xhr :get, :index
      expect(response.body.length).to eq(10666)
      expect(response.status).to eq(200)
    end
  end
end
