require 'rails_helper'

describe 'View charts', type: :feature, js: true do
  it 'should display all' do
    visit root_path

    expect(page).to have_content('Building session history charts')
    expect(page).to have_content('Builds quality per day')
    expect(page).to have_content('Duration vs. time')
    expect(page).to have_content('CyberCraft')
  end
end
