require 'rails_helper'

describe Build do

  it 'should read csv-file and return builds and abmormals' do
    model_respond = Build.read(Rails.root.join('spec', 'session_history.csv'))
    expect(model_respond[:builds][0].class).to eq(Build)
    expect(model_respond[:builds].length).to eq(108)
    expect(model_respond[:abnormals]).to be_a Hash
    expect(model_respond[:abnormals]['2014-08-23']).to be_truthy
    expect(model_respond[:abnormals]['2014-08-24']).to be_falsey
    expect(model_respond[:abnormals].reject { |k, v| !v }.length).to eq(2)
  end
end
