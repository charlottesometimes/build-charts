require "rails_helper"

RSpec.describe "app routes", type: :routing do
  it "should route to root path" do
    expect(get: root_path)
        .to route_to(
                controller: "index",
                action: "index"
            )
  end

  it "should route to api index path" do
    expect(get: api_path)
        .to route_to(
                controller: "api/builds",
                action: "index",
                format: "json"
            )
  end

end